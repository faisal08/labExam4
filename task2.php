<!DOCTYPE html>
<html>
<head lang="en">
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.3/jquery.min.js"></script>
    <script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
    <h2>Modal for Pondit.com</h2>
    <!-- Trigger the modal with a button -->
    <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target="#myModal">click for  Modal</button>

    <!-- Modal -->
    <div class="modal fade" id="myModal" role="dialog">
        <div class="modal-dialog">

            <!-- Modal content-->
            <div class="modal-content">
                <div class="modal-header">
                    <form action="dofile.php" method="post" enctype="multipart/form-data">
                        <div class="form-group">

                            <label>Description:</label>

                            <input type="text" class="form-control" name="description" placeholder="Description">
                        </div>
                        <div class="form-group">
                            <label>Date:</label>
                            <input type="date" class="form-control" name="date" placeholder="Enter Date">
                        </div>
                        <div class="form-group">
                            <label>Select image to Upload:</label>
                            <input type ="file" name="upload" id="filetoupload">
                        </div>
                        <div class="modal-footer">
                            <button type="submit" name="submit" class="btn btn-default">send</button>
                </div>
            </div>

        </div>
    </div>

</div>

</body>
</html>