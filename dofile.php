<?php
$description=$_POST['description'];
$date=$_POST['date'];
$name=time().$_FILES['upload']['name'];
$tmp=$_FILES['upload']['tmp_name'];
$picture=move_uploaded_file($tmp,'upload/'.$name);


?>
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
    <script src="bootstrap/js/jquery.min.js"></script>
    <script src="bootstrap/js/bootstrap.min.js"></script>
    <style>
        .carousel-inner > .item > img,
        .carousel-inner > .item > a > img {
            width: 70%;
            margin: auto;
        }
        .row{
            border: 2px solid red;
            width: 500px;
            margin: 150px;
            border-radius: 10px;


        }
    </style>
</head>
<body>

<div class="container">
    <br>
    <div class="row">
    <div id="myCarousel" class="carousel slide" data-ride="carousel">
        <!-- Indicators -->
        <ol class="carousel-indicators">
            <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
            <li data-target="#myCarousel" data-slide-to="1"></li>
        </ol>

        <!-- Wrapper for slides -->
        <div class="carousel-inner" role="listbox">

            <div class="item active">
                <img src="upload/<?php echo $name;?>" width="450" height="350" >
                <div class="carousel-caption">
                    <?php echo $description;
                    ?>
                </div>
            </div>
            <div class="item ">
                <img src="upload/<?php echo $name;?>" width="450" height="350" >
                <div class="carousel-caption">
                    <?php echo $description;
                    ?>
                </div>
            </div>

        </div>

        <!-- Left and right controls -->
        <a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
            <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
            <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>
</div>
    </div>

</body>
</html>
